//
//  CNFExchangesDataSource.m
//  CoinNotify
//

#import "CNFConstants.h"

#import "CNFExchangesDataSource.h"
#import "CNFExchangeItemEntity.h"
#import "CNFQuotesFetcher.h"

#import "NSFileManager+DirectoryLocations.h"


#define DATA_PATH [self pathToPlist]//[[NSFileManager defaultManager] applicationSupportDirectory]
#define PLIST_FILENAME @"de.C4CW.coinnotify.plist"


@interface CNFExchangesDataSource ()

/// variable to store exchange price
@property(nonatomic,strong) NSMutableArray *exchangePrice;

@end


@implementation CNFExchangesDataSource

+ (id) sharedDatasource
{
    static CNFExchangesDataSource *sharedDataSource = nil;
    @synchronized(self) {
        if (sharedDataSource == nil)
            sharedDataSource = [[super allocWithZone:NULL] init];
    }

    return sharedDataSource;
}

- (id) init
{
    self = [super init];
    self.exchangePrice = [NSMutableArray new];
    NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile: [DATA_PATH stringByAppendingPathComponent:PLIST_FILENAME]];
    NSLog(@"ITEMS IN ARRAY %lu",array.count);
    for (int i = 0; i < [array count]; i++) {
        [_exchangePrice insertObject:[CNFExchangeItemEntity fromDictionary:[array objectAtIndex:i]] atIndex:i];
    }
    
    return self;
}

- (void)addEntity:(id)anObject
{
    [_exchangePrice addObject:anObject];
    [self save];
    [self dispatchUpdatedNotification];
}

- (id)objectAtIndex:(NSUInteger)index
{
    return [_exchangePrice objectAtIndex:index];
}
- (NSUInteger)count
{
    return [_exchangePrice count];
}

- (void) updateQuotes
{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    for (int i = 0; i < [_exchangePrice count]; i++) {
        dispatch_async(queue, ^{
            CNFExchangeItemEntity *new = [CNFQuotesFetcher getQuotes:[_exchangePrice objectAtIndex:i]];
            //NSLog(@"%@: %@",new.symbol,new.priceString);
            dispatch_sync(dispatch_get_main_queue(), ^{
                //NSLog(@"LATER: %@: %@",new.symbol,new.priceString);
                if (new != nil) {
                    [_exchangePrice replaceObjectAtIndex:i withObject:new];
                    [self dispatchUpdatedNotification];
                }
            });
        });
    }
}

- (void) dispatchUpdatedNotification
{
    [[NSNotificationCenter defaultCenter] postNotification:[NSNotification notificationWithName:kUpdatedNotificationName object:nil]];
}

- (void)moveObjectFromIndex:(NSInteger) from toIndex:(NSInteger)to
{
    if (to != from) {
        id obj = [_exchangePrice objectAtIndex:from];
        [_exchangePrice removeObjectAtIndex:from];
        if (to >= [_exchangePrice count]) {
            [_exchangePrice addObject:obj];
        } else {
            [_exchangePrice insertObject:obj atIndex:to];
        }
        obj = nil;
        [self save];
    }
}

- (void)deleteObjectAtIndex:(NSUInteger)index
{
    [_exchangePrice removeObjectAtIndex:index];
    [self save];
}

- (BOOL) save
{
    NSMutableArray* newArray = [[NSMutableArray alloc] init];
    for (int i = 0; i < _exchangePrice.count; i++) {
        [newArray addObject:[((CNFExchangeItemEntity*)[_exchangePrice objectAtIndex:i]) toDictionary]];
    }
    BOOL result = [newArray writeToFile:[DATA_PATH stringByAppendingPathComponent:PLIST_FILENAME] atomically:YES];
//    if (result) {
//        NSLog(@":)");
//    } else {
//        NSLog(@":(");
//    }
    return result;
}

- (NSString*)pathToPlist{
    
    NSString *documentsFolder = [NSHomeDirectory() stringByAppendingPathComponent:@"Library"];
    documentsFolder =  [documentsFolder stringByAppendingPathComponent:@"Application Support"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentsFolder]) {
        //app is not sandboxed
        return documentsFolder;
    }else{
        
        return NSHomeDirectory();
    }
}

@end
