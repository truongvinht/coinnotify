//
//  CNFPopupViewController.m
//  CoinNotify
//

#import "CNFConstants.h"

#import "CNFPopupViewController.h"
#import "CNFExchangesDataSource.h"
#import "CNFExchangePriceCellView.h"
#import "CNFExchangeItemEntity.h"
#import "CNFExchangesDataSource.h"
#import "CNFAddNewCurrencyWindowController.h"
#import "CNFSettingsWindowController.h"

@implementation CNFPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    [[NSNotificationCenter defaultCenter] addObserverForName:kUpdatedNotificationName
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification){
                                                      [_pricesTableView reloadData];
                                                      
                                                      // method to resize the view
                                                      [self resizePopup];
                                                  }];
    [self loadView];
    
    return self;
}

- (void) awakeFromNib
{
    [_pricesTableView registerForDraggedTypes:[NSArray arrayWithObject:kPricesTableViewType]];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // method to resize the view
        [self resizePopup];
    });
}

- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    CNFExchangeItemEntity* item = [[CNFExchangesDataSource sharedDatasource] objectAtIndex:row];
    CNFExchangePriceCellView *cellView = [tableView makeViewWithIdentifier:@"MainCell" owner:self];

    cellView.symbol.stringValue = [item symbol];
    cellView.exchangeName.stringValue = [item exchangeName];

    if ([item priceString] == NULL) {
        //[cellView.price setHidden:YES];
        cellView.price.stringValue = @"No Data";
    } else {
        cellView.price.stringValue = [item priceString];
        
        [cellView.price setHidden:NO];
    }
    
    return cellView;
}

- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard *)pboard
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:rowIndexes];
    [pboard declareTypes:[NSArray arrayWithObject:kPricesTableViewType] owner:self];
    [pboard setData:data forType:kPricesTableViewType];
    
    return YES;
}

- (NSDragOperation)tableView:(NSTableView *)aTableView validateDrop:(id < NSDraggingInfo >)info proposedRow:(NSInteger)row proposedDropOperation:(NSTableViewDropOperation)operation
{
    if ([info draggingSource] == aTableView) {
        if (operation == NSTableViewDropOn){
            [aTableView setDropRow:row dropOperation:NSTableViewDropAbove];
        }
        return NSDragOperationMove;
    } else {
        return NSDragOperationNone;
    }
}

- (BOOL)tableView:(NSTableView *)aTableView acceptDrop:(id < NSDraggingInfo >)info row:(NSInteger)row dropOperation:(NSTableViewDropOperation)operation
{
    NSPasteboard* pboard = [info draggingPasteboard];
    NSData* rowData = [pboard dataForType:kPricesTableViewType];
    NSIndexSet* rowIndexes = [NSKeyedUnarchiver unarchiveObjectWithData:rowData];
    NSInteger from = [rowIndexes firstIndex];
    
    [[CNFExchangesDataSource sharedDatasource] moveObjectFromIndex:from toIndex:row];
    [aTableView reloadData];
    [[CNFExchangesDataSource sharedDatasource] dispatchUpdatedNotification];
    return YES;
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return [[CNFExchangesDataSource sharedDatasource] count];
}

/** Method to resize the popup*/
- (void)resizePopup{
    
    
    NSRect screenSize = [[NSScreen mainScreen] frame];
    
    //allow 3/4 of the height for the popup
    CGFloat minHeight = 200;
    CGFloat maxHeight = screenSize.size.height * 0.75;
    CGFloat cellheight = 47;
    
    NSUInteger numberOfItems = [[CNFExchangesDataSource sharedDatasource] count] + 1;
    
    //resize
    NSSize content = self.parent.contentSize;
    
    if (numberOfItems * cellheight < minHeight) {
        content.height =  minHeight;
    }else{
        if (numberOfItems * cellheight > maxHeight) {
            content.height =  maxHeight;
        }else{
            content.height = numberOfItems * cellheight;
        }
    }
    
    [self.parent setContentSize:content];
}

- (IBAction)openAddWindow:(id)sender
{
    if (_addCurrencyWindowController == nil) {
        _addCurrencyWindowController = [[CNFAddNewCurrencyWindowController alloc] initWithWindowNibName:@"CNFAddNewCurrencyWindowController"];
    }
    
    [_addCurrencyWindowController showWindow:sender];
    
}

- (IBAction)openSettings:(id)sender
{
    if (_settingWindowsController == nil) {
        _settingWindowsController = [[CNFSettingsWindowController alloc] initWithWindowNibName:@"CNFSettingsWindowController"];
    }
    [_settingWindowsController showWindow:sender];
}

- (IBAction)deleteRow:(id)sender
{
    [[CNFExchangesDataSource sharedDatasource] deleteObjectAtIndex:[_pricesTableView clickedRow]];
    [_pricesTableView reloadData];
    [[CNFExchangesDataSource sharedDatasource] dispatchUpdatedNotification];
    
    // method to resize the view
    [self resizePopup];
}

- (void)keyDown:(NSEvent *)theEvent{
    //check wether any cell was selected
    NSInteger row = [_pricesTableView selectedRow];
    
    if (row == -1) {
        return;
    }
    
    //remove item
    if (theEvent.keyCode==51||theEvent.keyCode==117) {
        [[CNFExchangesDataSource sharedDatasource] deleteObjectAtIndex:row];
        [_pricesTableView reloadData];
        [[CNFExchangesDataSource sharedDatasource] dispatchUpdatedNotification];
        
        // method to resize the view
        [self resizePopup];
    }
}

-(void)webView:(WebView *)webView decidePolicyForNewWindowAction:(NSDictionary *)actionInformation
       request:(NSURLRequest *)request
  newFrameName:(NSString *)frameName
decisionListener:(id <WebPolicyDecisionListener>)listener
{
    NSLog(@"Policy new window: %@", [request URL]);
    [listener ignore];
    [[NSWorkspace sharedWorkspace] openURL:[request URL]];

}
@end
