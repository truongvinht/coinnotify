//
//  CNFCSViewController.h
//  CoinNotify
//
//  Created by Vinh Tran on 02/01/15.
//  Copyright (c) 2015 Vinh Tran. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface CNFCSViewController : NSWindowController

@property (weak) IBOutlet NSTextField *textfield;

@property (weak) IBOutlet NSTextField *textoutput;
@end
