//
//  CNFQuotesFetcher.m
//  CoinNotify
//

#import "CNFQuotesFetcher.h"
#import "CNFExchangeItemEntity.h"

#define kPriceKey @"price"
#define kCurrencyKey @"currency"

@implementation CNFQuotesFetcher

+ (CNFExchangeItemEntity*) getQuotes:(CNFExchangeItemEntity*) item
{
    
    //limit cex.io
    if (item.exchangeId == CNCexIO) {
        NSUserDefaults *userprefs = [NSUserDefaults standardUserDefaults];
        NSDate *lastSync = [userprefs objectForKey:@"lastSync_CEXIO"];
        
        if (lastSync) {
            if([[NSDate date] timeIntervalSinceDate:lastSync]<15){
                NSLog(@"**ERROR TIMING ERROR** Cex.IO only allow 600 request within 10 mins");
                return nil;
            }
        }
        
        //remember last request
        [userprefs setObject:[NSDate date] forKey:@"lastSync_CEXIO"];
        [userprefs synchronize];
    }
    
    NSMutableURLRequest *quotesRequest = [NSMutableURLRequest requestWithURL:item.updateUrl
                                                              cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                              timeoutInterval:15.0];
    [quotesRequest addValue:[self getUserAgent] forHTTPHeaderField:@"User-Agent"];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *quotesData = [NSURLConnection sendSynchronousRequest:quotesRequest returningResponse:&response error:&error];
    
    if (quotesData == nil || error != nil) {
        NSLog(@"**ERROR GETTING QUOTES** (%@) with error: %@ ", [quotesRequest URL], error);
        return nil;
    }
    
    NSNumber* quotesResponse;
    
    switch (item.exchangeId) {
        case CNFMtGox:
            quotesResponse = [self parseMtGOX:quotesData];
            break;
        case CNFBitstamp:
            quotesResponse = [self parseBitstamp:quotesData];
            break;
        case CNFBtcE:
            quotesResponse = [self parseBtcE:quotesData];
            break;
        case CNFCampBX:
            quotesResponse = [self parseCampBX:quotesData];
            break;
        case CNFBitcoinAverage:
            quotesResponse = [self parseBitcoinAverage:quotesData];
            break;
        case CNFFxBTC:
            quotesResponse = [self parseFxBTC:quotesData];
            break;
        case CNFBTCChina:
            quotesResponse = [self parseBTCChina:quotesData];
            break;
        case CNCexIO:
            quotesResponse = [self parseCexIO:quotesData];
            break;
        case CNBittrex:
            quotesResponse = [self parseBittrex:quotesData];
            break;
        case CNCoinSwap:{
            quotesResponse = [self parseCoinSwap:quotesData];
        }
            break;
        case CNCryptsy:
            quotesResponse = [self parseCryptsy:quotesData];
            break;
        case CNCCex:
            quotesResponse = [self parseCCex:quotesData];
            break;
        default:
            return nil;
            break;
    }
    
    item.price = quotesResponse;
    NSString* price = [CNFExchangeItemEntity priceString:item.price withCurrency:item.currency_2];
    item.priceString = price;
    return item;
}

+ (NSNumber*) parseMtGOX:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"Error parsing MtGox data: %@", error);
        return nil;
    }
        
    float lastPrice = [(NSNumber*)[(NSDictionary*)[(NSDictionary*)[quotes objectForKey:@"data"] objectForKey:@"last"] objectForKey:@"value_int"] floatValue] / 100000;
    
    return [NSNumber numberWithFloat:lastPrice];
}

+ (NSNumber*) parseBitstamp:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing Bitstamp ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [quotes objectForKey:@"last"];
    return lastPrice;
}

+ (NSNumber*) parseBtcE:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing BtcE ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [(NSDictionary*)[quotes objectForKey:@"ticker"] objectForKey:@"last"];
    return lastPrice;
}

+ (NSNumber*) parseCampBX:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing CampBZ ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [quotes objectForKey:@"Last Trade"];
    return lastPrice;
}

+ (NSNumber*) parseBitcoinAverage:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];

    if (error != nil) {
        NSLog(@"** ERROR parsing BitcoinAverage ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [quotes objectForKey:@"last"];
    return lastPrice;
}

+ (NSNumber*) parseFxBTC:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing FxBTC ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [(NSDictionary*)[quotes objectForKey:@"ticker"] objectForKey:@"last_rate"];
    return lastPrice;
}

+ (NSNumber*) parseBTCChina:(NSData*) json
{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing BTCChina ticker**: %@", error);
    }
    
    NSString* lastPriceString = [(NSDictionary*)[quotes objectForKey:@"ticker"] objectForKey:@"last"];
    NSNumber* lastPrice = [NSNumber numberWithFloat:[lastPriceString floatValue]];
    
    return lastPrice;
}

+ (NSNumber*) parseCexIO:(NSData*)json{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing Cex.io ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [quotes objectForKey:@"lprice"];
    
    return lastPrice;
}

+ (NSNumber*) parseBittrex:(NSData*)json{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing Bittrex ticker**: %@", error);
    }
    
    NSNumber* lastPrice = [[quotes objectForKey:@"result"] objectForKey:@"Last"];
    
    return lastPrice;
}

+ (NSNumber*)parseCoinSwap:(NSData*)json{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    if (error != nil) {
        NSLog(@"** ERROR parsing CoinSwap ticker**: %@", error);
    }
    
    return [quotes objectForKey:@"lastprice"];
}

+ (NSNumber*)parseCryptsy:(NSData*)json{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    NSArray *dataList = [quotes objectForKey:@"data"];
    if ([dataList count]>0) {
        NSDictionary *data = [dataList firstObject];
        
        return [data objectForKey:@"tradeprice"];
    }
    return nil;
}

+ (NSNumber*)parseCCex:(NSData*)json{
    NSError *error;
    NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:json options:NSJSONReadingMutableContainers error:&error];
    
    NSDictionary *data = [quotes objectForKey:@"ticker"];
    return [data objectForKey:@"lastprice"];
}

+ (NSString*) getUserAgent
{
    return [NSString stringWithFormat:@"CoinNotify/%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
}

@end
