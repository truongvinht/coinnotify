//
//  CNFCSViewController.m
//  CoinNotify
//
//  Created by Vinh Tran on 02/01/15.
//  Copyright (c) 2015 Vinh Tran. All rights reserved.
//

#import "CNFCSViewController.h"

@interface CNFCSViewController ()

@property(nonatomic,strong) NSTimer *timer;

@property(nonatomic,strong) NSMutableURLRequest *request;
@end

@implementation CNFCSViewController

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:15 target:self selector:@selector(parse) userInfo:nil repeats:YES];
    
}

- (void)parse{
    
    if (!_request) {
        self.request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://api.coin-swap.net/allbalance/PovMDyBeE4N2bgGo"]
                                             cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                         timeoutInterval:10.0];
        
        NSURLResponse *response;
        
        NSData *quotesData = [NSURLConnection sendSynchronousRequest:_request returningResponse:&response error:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *quotes = [NSJSONSerialization JSONObjectWithData:quotesData options:NSJSONReadingMutableContainers error:nil];
            
            [self.textoutput setStringValue:[NSString stringWithFormat:@"%@",quotes]];
            self.request = nil;
        });
    }
}

@end
