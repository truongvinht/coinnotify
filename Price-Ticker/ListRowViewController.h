//
//  ListRowViewController.h
//  Price-Ticker
//
//  Created by Vinh Tran on 26/10/14.
//  Copyright (c) 2014 Vinh Tran. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ListRowViewController : NSViewController

@property (nonatomic,weak) IBOutlet NSTextField *textLabel;
@property (nonatomic,weak) IBOutlet NSTextField *textDescription;
@property (nonatomic,weak) IBOutlet NSTextField *textValue;

@property (nonatomic,strong) id data;

@end
