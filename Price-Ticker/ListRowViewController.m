//
//  ListRowViewController.m
//  Price-Ticker
//
//  Created by Vinh Tran on 26/10/14.
//  Copyright (c) 2014 Vinh Tran. All rights reserved.
//

#import "ListRowViewController.h"

#import "CNFExchangeItemEntity.h"

@implementation ListRowViewController

- (NSString *)nibName {
    return @"ListRowViewController";
}

- (void)loadView {
    [super loadView];

    // Insert code here to customize the view
    
    CNFExchangeItemEntity* item = _data;
    self.textLabel.stringValue = [item symbol];
    
    self.textDescription.stringValue = [item exchangeName];
  
    if ([item priceString] == NULL) {
        //[cellView.price setHidden:YES];
        self.textValue.stringValue = @"No Data";
    } else {
        self.textValue.stringValue = [item priceString];
        
        [self.textValue setHidden:NO];
    }
  
}

@end
