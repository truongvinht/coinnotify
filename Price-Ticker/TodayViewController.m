//
//  TodayViewController.m
//  Price-Ticker
//
//  Created by Vinh Tran on 26/10/14.
//  Copyright (c) 2014 Vinh Tran. All rights reserved.
//

#import "TodayViewController.h"
#import "ListRowViewController.h"
#import <NotificationCenter/NotificationCenter.h>

//parse data
#import "CNFExchangesDataSource.h"
#import "CNFExchangeItemEntity.h"
#import "CNFConstants.h"

@interface TodayViewController () <NCWidgetProviding, NCWidgetListViewDelegate, NCWidgetSearchViewDelegate>

@property (strong) IBOutlet NCWidgetListViewController *listViewController;
@property (strong) NCWidgetSearchViewController *searchController;

/// list with all currency labels
@property (nonatomic,strong) NSArray *currencyLabels;
@property (nonatomic,strong) NSArray *currencyObjects;

@property (nonatomic,readwrite) NSInteger notificationCounter;

@end


@implementation TodayViewController

#pragma mark - NSViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSUserDefaults standardUserDefaults] setObject:@(7) forKey:kSettingsDecimalSteps];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Set up the widget list view controller.
    // The contents property should contain an object for each row in the list.
    //self.listViewController.contents = @[@"Hello World!"];
    
    
    //NSDictionary *xample = @{@"symbol":@"RONG-SYMBOL",@"exchange":@"dasjjda"};
    
    self.listViewController.contents = @[];//[[CNFExchangesDataSource sharedDatasource] exchangePrice];//@[xample];
    
    
    self.listViewController.delegate = self;
    self.listViewController.hasDividerLines = YES;
    
    //init the currency list
    NSURL *currenciesURL = [[NSBundle mainBundle] URLForResource:@"currencies" withExtension:@"plist"];
    NSMutableArray *list = [[NSMutableArray alloc] initWithContentsOfURL:currenciesURL];
    [list sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"symbol" ascending:YES]]];
    
    self.currencyObjects = list;
    
    NSMutableArray *newList = [NSMutableArray new];
    
    for (int i=0; i < list.count; i++) {
        
        NSDictionary *currObject = [list objectAtIndex:i];
        
        [newList addObject:[NSString stringWithFormat:@"%@ (%@)",[currObject objectForKey:@"symbol" ],[currObject objectForKey:@"exchange"]]];
    }
    
    self.currencyLabels = newList;
    
    self.notificationCounter = 0;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kUpdatedNotificationName
                                                      object:nil
                                                       queue:nil
                                                  usingBlock:^(NSNotification *notification){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          
                                                          NSArray *items = [[CNFExchangesDataSource sharedDatasource] exchangePrice];
                                                          if (self.notificationCounter + 1 == [items count]) {
                                                              self.listViewController.contents = items;
                                                              self.notificationCounter=-1;
                                                          }else{
                                                              if(self.notificationCounter<=0){
                                                                  self.listViewController.contents=@[];
                                                              }
                                                              
                                                              self.notificationCounter++;
                                                          }
                                                      });
                                                      
                                                  }];
    
    [[CNFExchangesDataSource sharedDatasource] updateQuotes];
    
}

- (void)dismissViewController:(NSViewController *)viewController {
    [super dismissViewController:viewController];

    // The search controller has been dismissed and is no longer needed.
    if (viewController == self.searchController) {
        self.searchController = nil;
    }
}

#pragma mark - NCWidgetProviding

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult result))completionHandler {
    // Refresh the widget's contents in preparation for a snapshot.
    // Call the completion handler block after the widget's contents have been
    // refreshed. Pass NCUpdateResultNoData to indicate that nothing has changed
    // or NCUpdateResultNewData to indicate that there is new data since the
    // last invocation of this method.
    
    NSLog(@"Update content");
    
    completionHandler(NCUpdateResultNewData);
}

- (NSEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(NSEdgeInsets)defaultMarginInset {
    // Override the left margin so that the list view is flush with the edge.
    defaultMarginInset.left = 10;
    return defaultMarginInset;
}

- (BOOL)widgetAllowsEditing {
    // Return YES to indicate that the widget supports editing of content and
    // that the list view should be allowed to enter an edit mode.
    return YES;
}

- (void)widgetDidBeginEditing {
    // The user has clicked the edit button.
    // Put the list view into editing mode.
    self.listViewController.editing = YES;
}

- (void)widgetDidEndEditing {
    // The user has clicked the Done button, begun editing another widget,
    // or the Notification Center has been closed.
    // Take the list view out of editing mode.
    self.listViewController.editing = NO;
}

#pragma mark - NCWidgetListViewDelegate

- (NSViewController *)widgetList:(NCWidgetListViewController *)list viewControllerForRow:(NSUInteger)row {
    // Return a new view controller subclass for displaying an item of widget
    // content. The NCWidgetListViewController will set the representedObject
    // of this view controller to one of the objects in its contents array.
    
    ListRowViewController *item = [[ListRowViewController alloc] initWithNibName:@"ListRowViewController" bundle:nil];
    item.data = list.contents[row];
    
    
    CNFExchangeItemEntity *entity = item.data;
    NSLog(@"ITEM: %@ - %@",entity.symbol,entity.price);
    
    return item;
}

- (void)widgetListPerformAddAction:(NCWidgetListViewController *)list {
    // The user has clicked the add button in the list view.
    // Display a search controller for adding new content to the widget.
    self.searchController = [[NCWidgetSearchViewController alloc] init];
    self.searchController.delegate = self;

    // Present the search view controller with an animation.
    // Implement dismissViewController to observe when the view controller
    // has been dismissed and is no longer needed.
    [self presentViewControllerInWidget:self.searchController];
}

- (BOOL)widgetList:(NCWidgetListViewController *)list shouldReorderRow:(NSUInteger)row {
    // Return YES to allow the item to be reordered in the list by the user.
    return NO;
}

- (void)widgetList:(NCWidgetListViewController *)list didReorderRow:(NSUInteger)row toRow:(NSUInteger)newIndex {
    // The user has reordered an item in the list.
}

- (BOOL)widgetList:(NCWidgetListViewController *)list shouldRemoveRow:(NSUInteger)row {
    // Return YES to allow the item to be removed from the list by the user.
    return YES;
}

- (void)widgetList:(NCWidgetListViewController *)list didRemoveRow:(NSUInteger)row {
    // The user has removed an item from the list.
    
    [[CNFExchangesDataSource sharedDatasource] deleteObjectAtIndex:row];
    
}

#pragma mark - NCWidgetSearchViewDelegate

- (void)widgetSearch:(NCWidgetSearchViewController *)searchController searchForTerm:(NSString *)searchTerm maxResults:(NSUInteger)max {
    // The user has entered a search term. Set the controller's searchResults property to the matching items.
    
    NSIndexSet *matchIndexes = [_currencyLabels indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [[obj lowercaseString] rangeOfString:[searchTerm lowercaseString]].location != NSNotFound;
    }];
    
    searchController.searchResults = [_currencyLabels objectsAtIndexes:matchIndexes];
}

- (void)widgetSearchTermCleared:(NCWidgetSearchViewController *)searchController {
    // The user has cleared the search field. Remove the search results.
    searchController.searchResults = nil;
}

- (void)widgetSearch:(NCWidgetSearchViewController *)searchController resultSelected:(id)object {
    // The user has selected a search result from the list.
    
    NSUInteger position = [_currencyLabels indexOfObject:object];
    NSDictionary *dataObject = [_currencyObjects objectAtIndex:position];
    
//    NSMutableArray *oldItems = [NSMutableArray arrayWithArray:self.listViewController.contents];
//    [oldItems addObject:dataObject];
//    
//    self.listViewController.contents = oldItems;
    
    
    [[CNFExchangesDataSource sharedDatasource] addEntity:[CNFExchangeItemEntity fromDictionary:dataObject]];
    [[CNFExchangesDataSource sharedDatasource] updateQuotes];
}

@end
