# CoinNotify

This app was original forked from ibittler on Oct 2014.

Origin: http://ibittler.github.io/CoinNotify/

# Support

System:
Mac OS X 10.10 (yosemite) or higher

Source:

- BTC-E
- BTCChina
- BitcoinAverage
- Bitstamp
- Bittrex
- CampBX
- Cex.io
- Coin-Swap

#Warning
Some API Calls are restricted

- Cex.io


#License
MIT-License

#new in Version 1.0.3
- implement Mac OS X Widget to display prices
- allow larger popup in the main application to prevent scrolling
